using System;
using System.Collections.Generic;
using System.Linq;
using Interfaces;
using UnityEngine;

public class FillControl : MonoBehaviour, IStat
{
    [SerializeField] private List<PipesControl> pipes;
    [SerializeField] private int capacity;
    [SerializeField] private GameObject fluidGameObject;
    [SerializeField] private string sucsessString;
    [SerializeField] private string defeetString;
    [SerializeField] private GameObject tablet;

    private float _playTimeStart;
    private MeshRenderer _renderer;
    private float _curentCount = 0;
    private float _curentTime = 0;
    private float _startFluidLevel;
    private bool _isGameFinished = false;
    public Action OnGameEnd { get; set; }

    private void Start()
    {
        _renderer = fluidGameObject.GetComponent<MeshRenderer>();
        _startFluidLevel = fluidGameObject.transform.position.y;
        pipes.ForEach(pipe => pipe.OnCapacityChanged += ChangedCapacity);
    }

    private void ChangedCapacity(float value)
    {
        _curentCount += value;
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Tab))
        {
            tablet.SetActive(true);
        }
        if (_isGameFinished) return;
        var curLevelGameObject = Mathf.Lerp(fluidGameObject.transform.position.y,
            -_curentCount / capacity * _startFluidLevel + _startFluidLevel, 0.1f);

        fluidGameObject.transform.position = new Vector3(fluidGameObject.transform.position.x, curLevelGameObject,
            fluidGameObject.transform.position.z);

        Color color = _renderer.material.color;

        if (_curentCount == 0)
        {
            return;
        }

        if (_playTimeStart == 0)
            _playTimeStart = Time.time;

        _renderer.material.color = pipes.Aggregate(color,
            (current, pipe) => Color.Lerp(current, pipe.FluidColor, pipe.CurCapacity / _curentCount));
        if (_curentCount >= capacity)
        {
            _isGameFinished = true;
            pipes.ForEach(pipe =>
            {
                pipe.CloseValve();
                tablet.SetActive(true);
                OnGameEnd?.Invoke();
            });
        }
       
    }


    public string GetStats()
    {
        return pipes.Aggregate($"Ёмкость {capacity} л\n",
            (current, pipe) => current + $"{pipe.ColorName}-{(int)pipe.CurCapacity} л\n");
    }

    public string GetEndGameStat()
    {
        return pipes.Aggregate($"Ёмкость {capacity} л\nВремя {(int)(Time.time - _playTimeStart)} c\n" +
                               $"{(pipes.TrueForAll(pipe => (int)(pipe.CurCapacity / _curentCount) > pipe.Target - 5 && (int)(pipe.CurCapacity / _curentCount) < pipe.Target + 5) ? sucsessString : defeetString)}\n",
            (current, pipe) =>
                current +
                $"{pipe.ColorName}-{(int)(pipe.CurCapacity / _curentCount * 100)}%, {pipe.OperationCount} операций\n");
    }
}