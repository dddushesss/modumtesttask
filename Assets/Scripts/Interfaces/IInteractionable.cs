using UnityEngine;

namespace Interfaces
{
    internal interface IInteractionable
    {
        public string InteractString { get; set; }
        public bool IsVisible { get; set; }
        public Canvas UIGameObject { get;  }
        public bool IsUsing { get; set; }
        public void Interact();
        public void ShowInteractUI();
        public void HideUI();

    }
}