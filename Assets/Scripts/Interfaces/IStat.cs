﻿using System;

namespace Interfaces
{
    public interface IStat
    {
        public string GetStats();
        public string GetEndGameStat();
        public Action OnGameEnd { get; set; }
    }
}