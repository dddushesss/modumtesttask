﻿using System.Collections.Generic;
using System.Linq;
using Interfaces;
using TMPro;
using UnityEngine;

public class Score : MonoBehaviour
{
    [SerializeField] private TMP_Text _statText;
    [SerializeField] private PlayerControl _playerControl;
    private List<IStat> _stats;
    private bool _isGameFinished = false;
    private void Start()
    {
        _stats = new List<IStat>();
        _stats.AddRange(FindObjectsOfType<MonoBehaviour>().OfType<IStat>());
        _stats.ForEach(stat => stat.OnGameEnd += OnGameEnd);
    }

    private void OnGameEnd()
    {
        _statText.text = "";
        _playerControl.SetFreezePlayer(true);
        _stats.ForEach(stat =>
        {
            _statText.text += $"{stat.GetEndGameStat()}\n";
        });
        _isGameFinished = true;
    }
    private void Update()
    {
        if(_isGameFinished) return;
        _statText.text = "";

        _stats.ForEach(stat =>
        {
            _statText.text += $"{stat.GetStats()}\n";
        });
    }
}