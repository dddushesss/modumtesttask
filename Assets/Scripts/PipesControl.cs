using System;
using UnityEngine;

public class PipesControl : MonoBehaviour
{
    [SerializeField] private ParticleSystem fluidParticleSystem;
    [SerializeField] private RotateValve valve;
    [SerializeField] private Color fluidColor;
    [SerializeField] private string colorName;
    [SerializeField] private float fullThrotle;
    [SerializeField] private int target;
    public int OperationCount => valve.OperationCount;
    private float _curCapacity;

    public int Target => target;

    public Color FluidColor => fluidColor;

    public float CurCapacity => _curCapacity;

    public string ColorName => colorName;

    private float _throtle;


    private ParticleSystem.EmissionModule _emissionModule;

    public event Action<float> OnCapacityChanged;


    private void Start()
    {
        _curCapacity = 0;
        valve.OnValveRotate += ChangeValveValue;
        _emissionModule = fluidParticleSystem.emission;

        _emissionModule.rateOverTime = 0;
        fluidParticleSystem.GetComponent<Renderer>().material.color = fluidColor;
        fluidParticleSystem.Play();

    }

    private void ChangeValveValue(float value)
    {
        _throtle = value * fullThrotle;
        _emissionModule.rateOverTime = value * fullThrotle * 10;
    }

    private void Update()
    {
        _curCapacity += _throtle;
        OnCapacityChanged?.Invoke(_throtle);
    }

    public void CloseValve()
    {
        valve.CloseValve();
    }
}