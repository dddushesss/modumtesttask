using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using Interfaces;
using UnityEngine;
using Color = UnityEngine.Color;

[RequireComponent(typeof(Rigidbody), typeof(CapsuleCollider))]
public class PlayerControl : MonoBehaviour
{
    [SerializeField] private Transform toRotate;
    [SerializeField] private float mouseSensivity;
    [SerializeField] private float moveSpeed;
    [SerializeField] private float distaceToInteract;
   

    private IInteractionable _lastPointedInteraction;

    private bool _isFreeze = true;

    private Vector3 MoveDirection =>
        (Input.GetAxis("Vertical") * transform.right - Input.GetAxis("Horizontal") * transform.forward) * moveSpeed;

    private Vector3 LookDirection =>
        new Vector3(Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0) * mouseSensivity;

    private Rigidbody _rigidbody;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.freezeRotation = true;
        

    }

    private void FixedUpdate()
    {
        if (!_isFreeze)
        {
            _rigidbody.velocity = MoveDirection;
            transform.RotateAround(transform.position, transform.up, LookDirection.y);

            if (toRotate.eulerAngles.x < 80 + LookDirection.x && toRotate.eulerAngles.x >= 0 ||
                toRotate.eulerAngles.x < 360 && toRotate.eulerAngles.x > 280 + LookDirection.x)
                toRotate.RotateAround(transform.position, transform.forward, LookDirection.x);
        }
    }

    private void Update()
    {
        if (Physics.Raycast(toRotate.position, toRotate.TransformDirection(Vector3.forward), out var hit,
                distaceToInteract) &&
            hit.collider.gameObject.GetComponent<IInteractionable>() != null)
        {
            _lastPointedInteraction = hit.collider.gameObject.GetComponent<IInteractionable>();
            if (!_lastPointedInteraction.IsVisible)
            {
                _lastPointedInteraction.ShowInteractUI();
                _lastPointedInteraction.IsVisible = true;
            }

            if (Input.GetKeyDown(KeyCode.E) && _lastPointedInteraction is { IsUsing: false })
            {
                _lastPointedInteraction.Interact();
                _lastPointedInteraction.HideUI();
                ToogleFreezePlayer();
            }
            else if (Input.GetKeyDown(KeyCode.E) && _lastPointedInteraction is { IsUsing: true })
            {
                ToogleFreezePlayer();
                _lastPointedInteraction.ShowInteractUI();
                _lastPointedInteraction.IsUsing = false;
            }
        }

        else if (_lastPointedInteraction is { IsUsing: false })
        {
            _lastPointedInteraction.HideUI();
            _lastPointedInteraction.IsVisible = false;
        }

       

#if UNITY_EDITOR
        Debug.DrawRay(toRotate.position, toRotate.TransformDirection(Vector3.forward) * distaceToInteract, Color.red);
#endif
    }

    public void SetFreezePlayer(bool isFrezing)
    {
        _isFreeze = isFrezing;
    }
    public void ToogleFreezePlayer()
    {
        _isFreeze = !_isFreeze;
    }
}