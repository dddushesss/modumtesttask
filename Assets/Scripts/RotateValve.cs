using System;
using System.Collections;
using System.Collections.Generic;
using Interfaces;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class RotateValve : MonoBehaviour, IInteractionable
{
    [SerializeField] private Transform rotetablePart;
    [SerializeField, Range(0, 720)] private int rotationAngle;
    [SerializeField] private string interactString;
    [SerializeField] private Canvas uiGameObject;
    [SerializeField] private float rotationSpeed;

    [SerializeField] private ParticleSystem tube;
    public event Action<float> OnValveRotate;

    public int OperationCount { get; private set; }
    
    public string InteractString
    {
        get => interactString;
        set { interactString = value; }
    }

    public bool IsVisible { get; set; }
    public Canvas UIGameObject => uiGameObject;
    public bool IsUsing { get; set; }

    private Camera _camera;
    private Vector3 _previousHitPos;
    private float _valveRotation;
    private bool _isChangingValveValue;

    private void Start()
    {
        _camera = Camera.main;
        uiGameObject.worldCamera = _camera;
        uiGameObject.GetComponentInChildren<TMP_Text>().text = interactString;
        OperationCount = 0;
        uiGameObject.gameObject.SetActive(false);
    }

    public void Interact()
    {
        IsUsing = true;
    }

    public void CloseValve()
    {
        rotetablePart.eulerAngles = Vector3.zero;
        _valveRotation = 0;
        OnValveRotate?.Invoke(0);
    }

    private void OnMouseDrag()
    {
        if (!IsUsing || !Physics.Raycast(_camera.ScreenPointToRay(Input.mousePosition), out var hit)) return;

        _isChangingValveValue = true;
        if (_previousHitPos != Vector3.zero)
        {
            var curPoint = new Vector2(hit.point.x, hit.point.z);
            var prevPoint = new Vector2(_previousHitPos.x, _previousHitPos.z);
            float rotate = Vector2.SignedAngle(curPoint,
                prevPoint);


            if (curPoint.y - rotetablePart.position.z <= 0.01)
            {
                rotate = -rotate;
            }

            if (_valveRotation < 0)
            {
                _valveRotation = 0;
                rotetablePart.rotation = Quaternion.Euler(0,0,0);
                return;
            }

            if (_valveRotation > rotationAngle)
            {
                _valveRotation = rotationAngle;
                rotetablePart.rotation = Quaternion.Euler(0,rotationAngle,0);
                return;
            }
            
            rotetablePart.Rotate(Vector3.up,
                rotate * rotationSpeed);
            OnValveRotate?.Invoke(_valveRotation / rotationAngle);
            _valveRotation += rotate * rotationSpeed;
        }

        _previousHitPos = hit.point;
    }


    private void Update()
    {
        uiGameObject.transform.LookAt(_camera.transform.position);
        if (_isChangingValveValue && Input.GetMouseButtonUp(0))
        {
            OperationCount++;
        }
    }

    public void ShowInteractUI()
    {
        uiGameObject.gameObject.SetActive(true);
    }

    public void HideUI()
    {
        uiGameObject.gameObject.SetActive(false);
    }
}